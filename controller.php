<?php

require_once "settings.php";
require_once "core/db.php";
require_once "core/pattern.php";
require_once "core/collector.php";
require_once "core/view.php";

$error = True;

$solicitud = $_SERVER['REQUEST_URI'];
if($solicitud == "/") $solicitud = DEFAULT_URI;
@list($null, $archivo, $recurso, $arg) = explode('/', $solicitud);

if(in_array($archivo, $modulos_instalados)) {
    require_once "modules/$archivo.php";
    $controller_name = ucwords($archivo) . "Controller";
    if(method_exists($controller_name, $recurso)) {
        $error = False;
        $controller = new $controller_name();
        $controller->$recurso($arg);
    }
}

if($error) header("Location: " . DEFAULT_RESOURCE);
?>

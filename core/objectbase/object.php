
<?php
require_once 'core/objectbase/logicalconnector.php';

abstract class Object {

    function select() {
        extract(ObjectHelper::get_select_temp_vars($this));
        $compound_properties = ObjectHelper::get_compound_properties($this);
        $collector_properties = ObjectHelper::get_clasified_collector_properties($this);

        $sql = "SELECT $fields FROM $lower_class WHERE {$lower_class}_id = ?"; 
        $sql_bind_values = array($this->{"{$lower_class}_id"});
        $resultset = consultar_db($sql, $sql_bind_values);

        foreach($resultset[0] as $property=>$value) $this->$property = $value;

        foreach($compound_properties as $property) {
            $this->$property = Pattern::factory(
                ucwords($property), $this->$property);
        }

        ObjectHelper::compose_dependent_objects_properties(
            $collector_properties['dependent'], $this);

        ObjectHelper::compose_nondependent_objects_properties(
            $collector_properties['nodependent'], $this);
    }

    function insert() {
        extract(ObjectHelper::get_insert_temp_vars($this));
        $sql = "INSERT INTO $lower_class ($fields) VALUES ($marks)";

        $sql_bind_values = ObjectHelper::get_sql_bind_values($this);
        $this->{"{$lower_class}_id"} = consultar_db($sql, $sql_bind_values);

        $properties = ObjectHelper::get_clasified_collector_properties($this);
        foreach($properties['nodependent'] as $property) {
            $composite_class = ucwords(str_replace('_collection', '', $property));
            $cl = new LogicalConnector($this, $composite_class);
            $cl->save();
        }
    }

    function update() {
        extract(ObjectHelper::get_update_temp_vars($this));
        $sql = "UPDATE $lower_class SET $fields_marks WHERE {$lower_class}_id = ?";

        $sql_bind_values = ObjectHelper::get_sql_bind_values($this);
        $sql_bind_values[] = $this->{"{$lower_class}_id"}; 

        consultar_db($sql, $sql_bind_values);

        $properties = ObjectHelper::get_clasified_collector_properties($this);
        foreach($properties['nodependent'] as $property) {
            $composite_class = ucwords(str_replace('_collection', '', $property));
            $cl = new LogicalConnector($this, $composite_class);
            $cl->save();
        }
    }

    function delete() {
        $lower_class = ObjectHelper::get_lower_class($this);
        $sql = "DELETE FROM $lower_class WHERE {$lower_class}_id = ?";
        $sql_bind_values = array($this->{"{$lower_class}_id"});
        consultar_db($sql, $sql_bind_values);
    }

}

?>

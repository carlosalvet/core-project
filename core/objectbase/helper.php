<?php

class ObjectHelper {

    static function get_lower_class($object) {
        $class = get_class($object);
        $lower_class = strtolower($class);
        return $lower_class;
    }

    static function get_select_temp_vars($object) {
        $lower_class = self::get_lower_class($object);
        $simple_properties = self::get_simple_properties($object);

        $fields = implode(", ", $simple_properties);
        return get_defined_vars();
    }
    
    static function get_insert_temp_vars($object) {
        $lower_class = self::get_lower_class($object);
        $simple_properties = self::get_simple_properties($object);
        $fields = implode(", ", $simple_properties);

        $question_marks = array_fill(0, count($simple_properties), "?");
        $marks = implode(", ", $question_marks);

        return get_defined_vars();
    }

    static function get_update_temp_vars($object) {
        $lower_class = self::get_lower_class($object);
        $simple_properties = self::get_simple_properties($object);
        $prepared_statements = preg_replace('/$/', ' = ?', $simple_properties);
        $fields_marks = implode(", ", $prepared_statements);

        return get_defined_vars();
    }

    static function is_dependent_objects_property($property_name, $compound) {
        $compound_name = strtolower($compound);

        $composite_name = str_replace('_collection', '', $property_name);
        $composite_class = ucwords($composite_name);
        $is = property_exists(new $composite_class, $compound_name);

        return $is;
    }

    static function get_sql_bind_values($object) {
        $sql_bind_values = array();
        foreach($object as $p=>$value) {
            if(strpos($p, '_id') || is_array($value)) continue;
            $sql_bind_values[] = is_object($value) ? $value->{"{$p}_id"} : $value;
        }

        return $sql_bind_values;
    }

    static function get_simple_properties($object) {
        $simple_properties = [];
        foreach($object as $p=>$value) {
            if(strpos($p, '_id') || is_array($value)) continue;
            $simple_properties[] = $p;
        }
        return $simple_properties;
    }

    static function get_compound_properties($object) {
        $compound_properties = [];
        foreach($object as $property=>$value) {
            if(is_null($value)) $compound_properties[] = $property;
        }
        return $compound_properties;
    }

    static function get_collector_properties($object) {
        $collector_properties = array();
        foreach($object as $property=>$value) {
            if(is_array($value)) $collector_properties[] = $property;
        }
        return $collector_properties;
    }

    static function get_clasified_collector_properties($object) {
        $clasified_collectors = array(
            'dependent'=>array(), 
            'nodependent'=>array()
        );
        $collector_properties = self::get_collector_properties($object);
        foreach($collector_properties as $property) {
            $is = ObjectHelper::is_dependent_objects_property(
                $property, get_class($object));
            if($is){ 
                $clasified_collectors['dependent'][] = $property; 
            } else {
                $clasified_collectors['nodependent'][] = $property;
            }
        }
        return $clasified_collectors;
    }

    static function compose_dependent_objects_properties($properties, $object) {
        foreach($properties as $property) {
            $composite_name = str_replace('_collection', '', $property);
            $composite_class = ucwords($composite_name);
            $composite_id = "{$composite_name}_id";

            $compound_name = self::get_lower_class($object); 
            $compound_id = $object->{"{$compound_name}_id"};
            $composites = $composite_class::get($compound_id, $compound_name);
            foreach($composites as $array) {
                $obj_composite = new $composite_class();
                $obj_composite->$composite_id = $array[$composite_id];
                $obj_composite->select();

                $object->{"add_{$composite_name}"}($obj_composite);
            }        
        }
    }

    static function compose_nondependent_objects_properties($properties, $object) {
        foreach($properties as $property) {
            $composite_name = str_replace('_collection', '', $property);
            $cl = new LogicalConnector($object, $composite_name);
            $cl->get();
        }
    }
}

?>

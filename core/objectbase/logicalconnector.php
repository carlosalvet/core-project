<?php

class LogicalConnector {

    static $composite_name = '';

    function __construct($compound, $composite_class) {
        $this->compound = $compound;
        $this->composite = $this->get_collection($composite_class);
        $this->fm = 0;
    }

    function get() {
        $compound_name = strtolower(get_class($this->compound));
        $table = self::$composite_name . $compound_name;
        $sql = "SELECT  composite, fm
                FROM    $table
                WHERE   compound = ?
        ";
        $compound_id = "{$compound_name}_id";
        $sql_bind_values = array($this->compound->$compound_id);
        $resultset = consultar_db($sql, $sql_bind_values);

        $composite_class = ucwords(self::$composite_name);
        foreach($resultset as $asoc) {
            $composite = new $composite_class();
            $composite_id = self::$composite_name . "_id";
            $composite->$composite_id = $asoc['composite'];
            $composite->select();
            $collection_name = self::$composite_name . "_collection";
            $this->compound->$collection_name[] = $composite;
            $composite->fm = $asoc['fm'];
        }
    }

    function save() {
        $this->destroy();
        $compound_name = strtolower(get_class($this->compound));
        $table = self::$composite_name . $compound_name;
        $sql = "INSERT INTO $table 
                (compound, composite, fm)
                VALUES
                {values}
        ";
        $compound_id = "{$compound_name}_id";
        $values = array();
        $sql_bind_values = array();
        foreach($this->composite as $composite) {
            $values[] = "(?, ?, ?)";
            $composite_id = self::$composite_name . "_id";
            $data = array($this->compound->$compound_id, 
                $composite->$composite_id, $composite->fm);
            $sql_bind_values = array_merge($sql_bind_values, $data);
        }
 
        $values = implode(", ", $values);
        $sql = str_replace("{values}" , $values, $sql);
        consultar_db($sql, $sql_bind_values);
    }

    function destroy() {
        $compound_name = strtolower(get_class($this->compound));
        $table = self::$composite_name . $compound_name;
        $sql = "DELETE FROM $table WHERE compound = ?";
        $compound_id = "{$compound_name}_id";
        $sql_bind_values = array($this->compound->$compound_id);
        consultar_db($sql, $sql_bind_values);
    }

    protected function set_composite_name($composite_class){
        self::$composite_name = strtolower($composite_class);
    }

    protected function get_collection($composite_class) {
        $this->set_composite_name($composite_class);
        $collection_name = self::$composite_name . '_collection';
        return $this->compound->$collection_name;
    }

}

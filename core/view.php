<?php

abstract class View {

    private function set_dict(&$dict) {
        $constants = @get_defined_constants(True)['user'];
        $dict = $dict + $constants;
        $claves = array_keys($dict);
        $values = array_values($dict);
        foreach($claves as &$k) $k = "{{$k}}";
        $dict = array_combine($claves, $values);
    }

    public function render($template, $dict=array()){
        $this->set_dict($dict);
        return str_replace(
            array_keys($dict),
            array_values($dict),
            $template
        );
    }
}

<?php
/**
* Clase que provee de diferentes patrones de diseño
*
* @package    EuropioEngine
* @subpackage core.helpers
* @license    http://www.gnu.org/licenses/gpl.txt  GNU GPL 3.0
* @author     Eugenia Bahit <ebahit@member.fsf.org>
* @link       http://www.europio.org
*/

class Pattern {

    /**
    * Fabrica un objeto (Factory Design Pattern)
    *
    * @param  string $cls Nombre de la clase
    * @param  int $id_value ID del objeto a ser recuperado
    * @param  string $idname (opcional) Nombre de la propiedad ID del objeto
    * @return object Objeto fabricado
    */
    public static function factory($cls, $id_value, $idname='') {
        $objid = ($idname) ? $idname : strtolower($cls) . "_id";
        $obj = new $cls();
        $obj->$objid = $id_value;
        $obj->select();
        return $obj;
    }

    /**
    * Compone un objeto (Composite Design Pattern)
    *
    * @param  string $cls Nombre de la clase
    * @param  string $obj Objeto compositor
    * @return object $obj si es una instancia de $cls o de lo contrario, error
    */
    public static function composite($cls, $obj) {
        try {
            if(!is_a($obj, $cls)) {
                $class_name = get_class($obj);
                throw new Exception("$class_name no es del tipo $cls");
            } else {
                return $obj;
            }
        } catch (Exception $exception) {
            print "Error fatal: " . $exception->getMessage();
            exit();
        }
    }

}

?>

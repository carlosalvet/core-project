<?php

class Collector {

    public $coleccion = array();
    
    function get($objeto) {
        extract($this->_get_names($objeto));
        $sql = "SELECT $id FROM $tbl";
        $resultados = consultar_db($sql);
        
        foreach($resultados as $fila) {
            $obj = new $cls();
            $obj->$id = $fila[$id];
            $obj->select();
            $this->coleccion[] = $obj;
            unset($obj);
        }
    }

    private function _get_names($objeto) {
        $cls = ucwords($objeto) . 'Model';
        $tbl = strtolower($objeto);
        $id = "{$tbl}_id";
        return get_defined_vars();
    }
}

?>

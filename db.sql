DROP DATABASE IF EXISTS proyectonucleo;
CREATE DATABASE proyectonucleo;

USE proyectonucleo;


CREATE TABLE IF NOT EXISTS indumentaria(
    indumentaria_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(128),
    precio DECIMAL(11, 2),
    talla VARCHAR(10)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS producto(
    producto_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(128),
    precio DECIMAL(11, 2)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS pais(
    pais_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(128)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS estado(
    estado_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(128),
    pais INT(11),
    INDEX(pais),
    FOREIGN KEY(pais)
        REFERENCES pais(pais_id)
        ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS domicilio(
    domicilio_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    calle VARCHAR(64),
    numero VARCHAR(15),
    planta INT(7),
    puerta VARCHAR(15),
    ciudad VARCHAR(20),
    cp VARCHAR(5),
    estado INT(11),
    INDEX(estado),
    FOREIGN KEY(estado)
        REFERENCES estado(estado_id)
        ON DELETE CASCADE
)ENGINE=InnoDB; 

CREATE TABLE IF NOT EXISTS cliente(
    cliente_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(128),
    rfc VARCHAR(15),
    domicilio INT(11),
    FOREIGN KEY(domicilio)
        REFERENCES domicilio(domicilio_id)
        ON DELETE SET NULL
)ENGINE=InnoDB; 

CREATE TABLE IF NOT EXISTS pedido(
    pedido_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    fecha DATE,
    estado INT(1),
    cliente INT(11),
    INDEX(cliente),
    FOREIGN KEY(cliente)
        REFERENCES cliente(cliente_id)
        ON DELETE CASCADE,
    domicilio INT(11),
    FOREIGN KEY(domicilio)
        REFERENCES domicilio(domicilio_id)
        ON DELETE SET NULL
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS datodecontacto(
    datodecontacto_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(128),
    valor VARCHAR(128),
    cliente INT(11),
    INDEX(cliente),
    FOREIGN KEY(cliente)
        REFERENCES cliente(cliente_id)
        ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS categoria(
    categoria_id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    denominacion VARCHAR(128)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS productopedido(
    compuesto INT(11),
    INDEX(compuesto),
    FOREIGN KEY(compuesto)
	REFERENCES pedido(pedido_id)
	ON DELETE CASCADE,
    compositor INT(11),
    FOREIGN KEY(compositor)
	REFERENCES producto(producto_id)
	ON DELETE SET NULL
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS productocategoria(
    compuesto INT(11),
    INDEX(compuesto),
    FOREIGN KEY(compuesto)
	REFERENCES categoria(categoria_id)
	ON DELETE CASCADE,
    compositor INT(11),
    FOREIGN KEY(compositor)
	REFERENCES producto(producto_id)
	ON DELETE SET NULL
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS categoriacategoria(
    compuesto INT(11),
    INDEX(compuesto),
    FOREIGN KEY(compuesto)
	REFERENCES categoria(categoria_id)
	ON DELETE CASCADE,
    compositor INT(11),
    FOREIGN KEY(compositor)
	REFERENCES categoria(categoria_id)
	ON DELETE SET NULL
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS pedidocliente(
    compuesto INT(11),
    INDEX(compuesto),
    FOREIGN KEY(compuesto)
        REFERENCES cliente(cliente_id)
        ON DELETE CASCADE,
    compositor INT(11),
    FOREIGN KEY(compositor)
        REFERENCES pedido(pedido_id)
        ON DELETE SET NULL
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `usuario` (
      `usuario_id` VARCHAR(8) NOT NULL PRIMARY KEY
    , `denominacion` VARCHAR(60)
    , `nivel` INT(2)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `credencial` (
     `credencial_id` VARCHAR(32) NOT NULL PRIMARY KEY
   , `activa` BOOL
) ENGINE=InnoDB;

<?php

require_once APP_PATH ."/".MODULES."/credencial.php";


class Usuario {

    function __construct() {
        $this->usuario_id = '';
        $this->denominacion = '';
        $this->nivel = 0;
    }

    function get() {
        $query = "SELECT denominacion, nivel FROM usuario WHERE usuario_id = ?";
        $datas = array($this->usuario_id);
        $resultset = consultar_db($query, $datas);

        $this->denominacion = $resultset[0]['denominacion'];
        $this->nivel = $resultset[0]['nivel'];
    }

    function insert() {
        $query = "INSERT INTO usuario(usuario_id, denominacion, nivel) VALUES (?, ?, ?)";
        $datas = array($this->usuario_id, $this->denominacion, $this->nivel);
        consultar_db($query, $datas);
    }

    function destroy() {

    }

    function update() {
    
    }
}


class UsuarioView {

    function obtener($usuario) {
    }
}


class UsuarioController {

    function __construct() {
        $this->view = new UsuarioView();
        $this->model = new Usuario;
    }

    function obtener() {
        $_POST = array(
            "usuario"=>"admin",
        );

        $uid = UsuarioHelper::set_uid("usuario");
        $usuario = new Usuario();
        $usuario->usuario_id = $uid;
        $usuario->get();

        $this->view->obtener($usuario);
    }
    
    function agregar() {
        $_POST = array(
            "usuario"=>"admin",
            "clave"=>"123456",
            "denominacion"=>"Juan Manúel Peréz",
            "nivel"=>1
        );

        //$_POST = array(
            //"usuario"=>"jorge",
            //"clave"=>"11111",
            //"denominacion"=>"Ricardo António Ruíz Cortines",
            //"nivel"=>2
        //);


        $this->model->usuario_id = UsuarioHelper::set_uid($_POST["usuario"]);
        $this->model->denominacion = $_POST["denominacion"];
        $this->model->nivel = $_POST["nivel"];
        $this->model->insert();

        $credencial = new Credencial();
        $credencial->credencial_id = CredencialHelper::set_credencial($_POST["usuario"], $_POST["clave"]);
        $credencial->activa = true;
        $credencial->save();

        echo 'solo una salida para ver que pasas';
    }

    function verificar() {
        $_POST = array(
            //"usuario"=>"admin",
            //"clave"=>"123456",
            "usuario"=>"jorge",
            "clave"=>"11111",
        );

        $credencial = CredencialHelper::set_credencial($_POST["usuario"], $_POST["clave"]);
        $credencial_file = FSO_PATH . "/credencial/{$credencial}";
        if(file_exists($credencial_file)) {
           $uid = UsuarioHelper::set_uid($_POST["usuario"]);
           $usuario = new Usuario();
           $usuario->usuario_id = $uid;
           $usuario->get();
           // La denominación pública del usuario es $usuario->denominacion
           echo "El usuario {$usuario->denominacion} ingresó correctamente";
        } else { 
           echo "El usuario {$usuario->denominacion} no tiene permisos suficienetes para ver esta página";
        }
    }
}


class UsuarioHelper {

       static function set_uid($username) {
           $user_hash = hash('md5', $username);
           $chars = substr($user_hash, -5);
           return hash('crc32', "{$user_hash}{$chars}");
       }
}


?>

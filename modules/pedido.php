<?php

require_once "modules/producto.php";
require_once "modules/domicilio.php";

class Pedido extends DependentObject {

    function __construct(Domicilio $domicilio=NULL) {
        $this->pedido_id = 0;
        $this->fecha = '';
        $this->estado = 0;
        $this->cliente = 0;
        $this->domicilio = $domicilio;
        $this->producto_collection = array();
    }

    function add_producto(Producto $producto) {
        $this->producto_collection[] = $producto;
    }

}


class PedidoView {

    function ver($object) {
        header('Content-Type: text/html; charset=UTF-8');
        echo '<pre>';
        print_r($object);
        echo '</pre>';
    }

    function agregar() {
        echo 'hola mundo desde pedido/agregar';
    } 

    function modificar() {
        echo 'hola mundo desde pedido/modificar';
    }

    function listar() {
        echo 'hola mundo desde pedido/listar';
    }

}


class PedidoController {

    function __construct() {
        $this->model = new Pedido();
        $this->view = new PedidoView();
    }

    function ver($id=0) {
        $this->model->pedido_id = $id;
        $this->model->select();

        $cl = new ProductoPedido($this->model);
        $cl->get();

        $this->view->ver($this->model);
    }

    function agregar() {
        $this->view->agregar();
    }

    function guardar() {
        $_POST['pedido']['fecha'] = '2017-10-24';
        $_POST['pedido']['estado'] = 3;
        $_POST['pedido']['cliente'] = 2;
        $_POST['pedido']['domicilio'] = 2;
        $_POST['producto'][0]['producto_id'] = 1;
        $_POST['producto'][1]['producto_id'] = 2;
        extract($_POST['pedido']);

        $this->model->fecha = $fecha;
        $this->model->estado = $estado;
        $this->model->cliente = $cliente;
        $this->model->domicilio = $domicilio;
        $this->model->insert();

        foreach($_POST['producto'] as $array) {
            $producto = new Producto();
            $producto->producto_id = $array['producto_id'];
            $producto->select();

            $this->model->add_producto($producto);
        }

        $cl = new ProductoPedido($this->model);
        $cl->save();
        header("Location:/pedido/ver/{$this->model->pedido_id}");
    }

    function modificar() {
        $this->view->modificar();
    }

    function actualidatodecontactoar() {
        $this->model->update();

        header("Location:/pedido/ver/{$this->model->pedido_id}");
    }

    function eliminar($id) {
        $this->model->pedido_id = $id;
        $this->model->delete();

        $cl = new ProductoPedido($this->model);
        $cl->destroy();

        header('Location:/pedido/listar');
    }

    function listar() {
        $this->view->listar();
    }

}


class ProductoPedido extends LogicalConnector{ }
?>

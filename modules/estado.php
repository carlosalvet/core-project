<?php

require_once "modules/domicilio.php";

class Estado extends DependentObject{

    function __construct() {
        $this->estado_id = 0;
        $this->denominacion = '';
        $this->pais = 0;
        $this->domicilio_collection = array();
    }

    /*function select() {
        $compositores = Domicilio::get($this->estado_id);
        foreach($compositores as $array) {
            $domicilio = new Domicilio();
            $domicilio->domicilio_id = $array['domicilio_id'];
            $domicilio->select();

            $this->add_domicilio($domicilio);
        }
    }*/

    function add_domicilio(Domicilio $domicilio) {
        $this->domicilio_collection[] = $domicilio;
    }

}


class EstadoView {

    function ver($object) {
        header('Content-Type: text/html; charset=UTF-8');
        echo '<pre>';
        print_r($object);
        echo '</pre>';
    }

    function agregar() {
        echo 'hola mundo desde estado/agregar';
    } 

    function modificar() {
        echo 'hola mundo desde estado/modificar';
    }

    function listar() {
        echo 'hola mundo desde estado/listar';
    }

}


class EstadoController {

    function __construct() {
        $this->model = new Estado();
        $this->view = new EstadoView();
    }

    function ver($id=0) {
        $this->model->estado_id = $id;
        $this->model->select();
        $this->view->ver($this->model);
    }

    function agregar() {
        $this->view->agregar();
    }

    function guardar() {
        $_POST = array(
            'denominacion'=>'Michoacán',
            'pais'=>1
        );
        extract($_POST);

        $this->model->denominacion = $denominacion;
        $this->model->pais = $pais;
        $this->model->insert();

        header("Location/estado/ver/{$this->model->estado_id}");
    }

    function modificar() {
        $this->view->modificar();
    }

    function actualizar() {
        $this->model->update();

        header("Location/estado/ver/{$this->model->estado_id}");
    }

    function eliminar($id) {
        $this->model->estado_id = $id;
        $this->model->delete();
        
        header('Location/estado/listar');
    }

    function listar() {
        $this->view->listar();
    }

}


?>

<?php

require_once "modules/estado.php";

class Domicilio extends DependentObject {

    # Se hace dependiente a los fines prácticos
    function __construct() {
        $this->domicilio_id = 0;
        $this->calle = '';
        $this->numero = '';
        $this->planta = 0;
        $this->puerta = '';
        $this->ciudad = '';
        $this->cp = '';
        $this->estado = 0;
    }

}


class DomicilioView {
    function ver() {
        echo 'hola mundo desde domicilio/ver';
    }

    function agregar() {
        echo 'hola mundo desde domicilio/agrgar';
    }

    function modificar() {
        echo 'hola mundo desde domicilio/modificar';
    }

    function listar() {
        echo 'hola mundo desde domicilio/listar';
    }

}


class DomicilioController
{
    function __construct() {
        $this->model = new Domicilio();
        $this->view = new DomicilioView();
    }

    function ver($id) {
        $this->view->ver();
    }

    function agregar() {
        $this->view->agregar();
    }

    function guardar() {
        extract($_POST['domicilio']);

        $this->model->calle = $calle;
        $this->model->numero = $numero;
        $this->model->planta = $planta;
        $this->model->ciudad = $ciudad;
        $this->model->puerta = $puerta;
        $this->model->cp = $cp;
        $this->model->estado = $estado;

        $this->model->insert();
        return $this->model;
    }

    function modificar() {
        $this->view->modificar();
    }

    function actualizar() {
        $this->model->update();

        header("Location:/domicilio/ver/{$this->model->domicilio_id}");
    }

    function eliminar() {
        $this->model->delete();

        header('Location:/domicilio/listar');
    }

    function listar() {
        $this->view->listar();
    }

    function get_domcilio_compositores() {
        $sql = "SELECT domicilio_id FROM domicilio WHERE estado = ?";
        $datos = array($this->estado);
        return consultar_db($sql, $datos);
    }

}


?>

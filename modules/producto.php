<?php

class Producto extends Object {

    public function __construct() {
        $this->producto_id = 0;
        $this->denominacion= '';
        $this->precio= 0.00;
    }

}




class ProductoView extends View {

    public static function del_errors($formulario) {
        $identificador = "errores";
        $regex = "/<!-- $identificador -->(.|\n){1,}<!-- $identificador -->/";
        $formulario = preg_replace($regex, "", $formulario);
        return $formulario;
    }

    public static function set_errors($error_fields, $formulario) {
        $arr_errores = array(
            "denomination"=>"Nombre del producto no puede ser nulo",
            "price"=>"price no puede ser menor a 1",
            "imagen"=>"La imagen debe ser JPG o PNG"
        );

        $mensajes = array();
        foreach($error_fields as $field) $mensajes[] = $arr_errores[$field];

        $msg = nl2br(implode("\n", $mensajes));
        $formulario = str_replace("{errores}", $msg, $formulario);
        return $formulario;
    }

    public function agregar($error_fds=array()) {
        $template = file_get_contents("static/template/template.html");
        $componentes = file_get_contents("static/template/template_completo.html");

        $identificador = "producto\.agregar";
        $regex = "/<!-- $identificador -->(.|\n){1,}<!-- $identificador -->/";
        var_dump($regex); exit;
        preg_match($regex, $componentes, $coincidencias);
        $formulario = @$coincidencias[0];
        $formulario = (!$error_fds) ? self::del_errors($formulario) : self::set_errors($error_fds, $formulario);
        $dict = [
            "titulo"=>"Aplicación de Carlos",
            "modulo"=>"PRODUCTOS",
            "contenido"=>$formulario,
        ];

        print $this->render($template, $dict);
    }

    public function ver($object=0) {
        $template = file_get_contents('static/template/template_ok.html');
        //$detalle_path = "../private/productos/detalles/{$object->producto_id}/detalle";
        //$detalle = file_get_contents($detalle_path);

        $object_dict = array(
            "titulo"=>"Producto Detalle",
            "modulo"=>"Detalle del producto",
            "producto_id"=>$object->producto_id,
            "denomination"=>$object->denomination,
            "price"=>$object->price,
            //"detalle"=>$detalle,
            //"imagen"=>"",
        );

        echo $this->render($template, $object_dict);
    }    

}


class ProductoController{

    public function __construct() {
        $this->model = new Producto();
        $this->view = new ProductoView();
    }

    public function agregar() {
        $this->view->agregar(array("denomination", "price", "imagen"));
    }

    public function guardar() {
        //$imagen_datos = $_FILES["imagen"];
        extract($_POST);

        $this->model->denomination = $denomination;
        $this->model->price = $price;
        $this->model->insert();

        /*$detalle_path = "../private/productos/detalles/{$this->model->producto_id}/";
        if(!is_dir($detalle_path)) mkdir($detalle_path, 0755, TRUE);
        file_put_contents($detalle_path."detalle", $detalle);

        $imagen_ppal_path = "../private/productos/imagenes/{$this->model->producto_id}/";
        ProductoHelper::upload_imagefile($imagen_datos, $imagen_ppal_path);*/

        header("Location:/producto/ver/{$this->model->producto_id}");
    }

    public function ver($id=0) {
        $this->model->producto_id = $id;
        $this->model->select();
                            
        $this->view->ver($this->model);
    }

}


class ProductoHelper{

    public static function upload_imagefile($imagen_datos,$imagen_ppal_path) {
        $image_formats_permitted = array("image/png", "image/jpg", "image/jpeg");
        $info = getimagesize($imagen_datos["tmp_name"]);
        $extension = image_type_to_extension($info[2]);
        $mime = $info['mime'];

        if(!is_dir($imagen_ppal_path)) mkdir($imagen_ppal_path, 0755, TRUE);

        if(!in_array($mime, $image_formats_permitted)) {
            return FALSE;
        }

        $name_path = $imagen_ppal_path . "principal{$extension}";
        move_uploaded_file($imagen_datos["tmp_name"], $name_path);
    }
            
    public static function create_dir_if_not_exists($dir) {
    
    }
}

?>

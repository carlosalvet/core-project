<?php

class DatoDeContacto extends DependentObject {

    function __construct() {
        $this->datodecontacto_id = 0;
        $this->denominacion = '';
        $this->valor = '';
        $this->cliente = 0;
    }

}


class DatoDeContactoView {

    function ver() {
        echo 'hola mundo desde datodecontacto/ver';
    }

    function agregar() {
        echo 'hola mundo desde datodecontacto/agregar';
    } 

    function modificar() {
        echo 'hola mundo desde datodecontacto/modificar';
    }

    function listar() {
        echo 'hola mundo desde datodecontacto/listar';
    }

}

class DatoDeContactoController{

    function __construct() {
        $this->model = new Datodecontacto();
        $this->view = new DatodecontactoView();
    }

    function ver() {
        $this->view->ver();
    }

    function agregar() {
        $this->view->agregar();
    }

    function guardar() {

        $_POST['denominacion'] = 'telefono';
        $_POST['valor'] = '1111222233';
        $_POST['cliente'] = 2; 

        extract($_POST);

        $this->model->denominacion = $denominacion;
        $this->model->valor = $valor;
        $this->model->cliente = $cliente;

        $this->model->insert();
        var_dump($this->model);
        return $this->model;
    }

    function guardar_todo($object) {
        $datodecontacto_array = $_POST['datodecontacto'];

        foreach($datodecontacto_array as $array) {
            $_POST['datodecontacto']['denominacion'] = $array['denominacion'];
            $_POST['datodecontacto']['valor'] = $array['valor'];
            $_POST['datodecontacto']['cliente'] = $object->cliente_id;
            $dc = $this->guardar();
            $object->add_datodecontacto(clone $dc);
        }
    }

    function modificar() {
        $this->view->modificar();
    }

    function actualizar() {
        $this->model->update();

        header('Location:/datodecontacto/ver');
    }

    function eliminar($id) {
        $this->model->delete();

        header('Location:/datodecontacto/listar');
    }

    function listar() {
        $this->view->listar();
    }

}


?>

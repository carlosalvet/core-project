<?php


class Credencial {

    function __construct() {
        $this->credencial_id = '';
    }

    function save() {
        $credencial_file = FSO_PATH . "/credencial/{$this->credencial_id}";
        file_put_contents($credencial_file, "");
    }

    function destroy() {}
}


class CredencialView {

}


class CredencialController {

    function __construct() {
        $this->view = new CredencialView();
        $this->model = new Credencial;
    }

}


class CredencialHelper {

     static function set_credencial($username, $password) {
         $user_hash = hash('sha512', $username);
         $pass_hash = hash('md4', $password);
         $salt_hash = hash('crc32', substr($user_hash, 5, 12));
         return hash('md5', "{$pass_hash}{$user_hash}{$salt_hash}");
     }
}
?>

<?php

require_once "modules/estado.php";
require_once "modules/domicilio.php";

class Pais extends Object {

    function __construct() {
        $this->pais_id = 0;
        $this->denominacion = '';
        $this->estado_collection = array();
    }

    /*function select() {
        $compositores = Estado::get($this->pais_id);
        foreach($compositores as $array){
            $estado = new Estado();
            $estado->estado_id = $array['estado_id'];
            $estado->select();

            $this->add_estado(clone $estado);
            unset($estado);
        }

    }*/

    function add_estado(Estado $estado) {
        $this->estado_collection[] = $estado;
    }

}


class PaisView {

    function ver($object=NULL) {
        header('Content-Type: text/html; charset=UTF-8');
        echo '<pre>';
        print_r($object);
        echo '</pre>';
    }

    function agregar() {
        echo 'hola mundo desde pais/agregar';
    }    

    function modificar() {
        echo 'hola mundo desde pais/modificar';
    }

    function listar() {
        echo 'hola mundo desde pais/listar';
    }

}


class PaisController {

    function __construct() {
        $this->model = new Pais();
        $this->view = new PaisView();
    }

    function ver($id=0) {
        $this->model->pais_id = $id;
        $this->model->select();

        $this->view->ver($this->model);
    }

    function agregar() {
        $this->view->agregar();
    }

    function guardar() {
        $_POST = array('denominacion'=>'Panamá');
        extract($_POST);

        $this->model->denominacion = $denominacion;
        $this->model->insert();

        header("Location:/pais/ver/{$this->model->pais_id}");
    }

    function modificar() {
        $this->view->modificar();
    }

    function actualizar() {
        $this->model->update();

        header("Location:/pais/ver/{$this->model->pais_id}");
    }

    function eliminar($id) {
        $this->model->pais_id = $id;
        $this->model->delete();

        header("Location:/pais/listar/");
    }

    function listar() {
        $this->view->listar();
    }

}


?>

<?php

require_once 'modules/producto.php';


class Indumentaria extends Producto {

    function __contruct() {
        $this->indumentaria_id = 0;
        parent::__construct();
        $this->talla = 0;
    }

    function select() {
        $sql = "
            SELECT  denominacion, precio, talla
            FROM    indumentaria
            WHERE   indumentaria_id = ?
        ";
        $datos = array($this->indumentaria_id);
        $resultados = consultar_db($sql, $datos);

        $this->denominacion = $resultado[0]['denominacion'];
        $this->precio = $resultado[0]['precio'];
        $this->talla = $resultado[0]['talla'];
    }

}


class IndumentariaView extends View {

    public static function del_errors($formulario) {
        $identificador = "errores";
        $regex = "/<!-- $identificador -->(.|\n){1,}<!-- $identificador -->/";
        $formulario = preg_replace($regex, "", $formulario);
        return $formulario;
    }
    
    public static function set_errors($error_fields, $formulario) {
        $arr_errores = array(
            "denominacion"=>"Nombre del producto no puede ser nulo",
            "precio"=>"Precio no puede ser menor a 1",
            "imagen"=>"La imagen debe ser JPG o PNG",
            "talla"=>"La Talla no es válida",
        );

        $mensajes = array();
        foreach($error_fields as $field) $mensajes[] = $arr_errores[$field];

        $msg = nl2br(implode("\n", $mensajes));
        $formulario = str_replace("{errores}", $msg, $formulario);
        return $formulario;

    }

    public function agregar($error_fds=array()) {
        $template = file_get_contents("static/template/template.html");

        # Armado de componentes
        $componentes = file_get_contents("static/template/template_completo.html");

        $identificador = "producto\.agregar";
        $regex = "/<!-- $identificador -->(.|\n){1,}<!-- $identificador -->/";
        preg_match($regex, $componentes, $coincidencias);
        $formulario = @$coincidencias[0];
        
        $formulario = (!$error_fds) ? self::del_errors($formulario) : self::set_errors($error_fds, $formulario);

        $dict = [
            "titulo"=>"Aplicación de Carlos",
            "modulo"=>"PRODUCTOS",
            "contenido"=>$formulario,
        ];

        print $this->render($template, $dict);
    }

    public function ver($object=0) {
        $template = file_get_contents('static/template/template_ok.html');
        $detalle_path = "../private/productos/detalles/{$object->producto_id}/detalle";
        $detalle = file_get_contents($detalle_path);

        $object_dict = array(
            "titulo"=>"Producto Detalle",
            "modulo"=>"Detalle del producto",
            "producto_id"=>$object->producto_id,
            "denominacion"=>$object->denominacion,
            "precio"=>$object->precio,
            "detalle"=>$detalle,
            "imagen"=>"",
        );

        echo $this->render($template, $object_dict);

    }

    public function modificar() {
        echo 'hola desde la vista indumentaria/modificar';
    }

    public function listar() {
        echo 'hola desde la vista indumentaria/listar';
    }
}


class IndumentariaController {

    public function __construct() {
        $this->model = new Indumentaria();
        $this->view = new IndumentariaView();
    }

    public function agregar() {
        $errors_fds = array("denominacion", "precio", "imagen");
        $this->view->agregar($errors_fds);
    }

    public function guardar() {
        $_POST = array(
            'denominacion'=>'Mèxico',
            'precio'=>12.00,
            'talla'=>'M'
        );

        extract($_POST);
        $imagen_datos = $_FILES["imagen"];

        $this->model->denominacion = $denominacion;
        $this->model->precio = $precio;
        $this->model->talla = $talla;
        $this->model->insert();

        $detalle_path = "../private/productos/detalles/{$this->model->producto_id}/";
        IndumentariaHelper::create_dir_if_not_exists($detalle_path);
        file_put_contents($detalle_path."detalle", $detalle);

        $imagen_ppal_path = "../private/productos/imagenes/{$this->model->producto_id}/";
        IndumentariaHelper::upload_imagefile($imagen_datos, $imagen_ppal_path);

        header("Location:/producto/ver/{$this->model->producto_id}");
    }

    public function ver($id=0) {
        $this->model->producto_id = $id;
        $this->model->select();
    
        $this->view->ver($this->model);
    }


    public function modificar() {
        $this->view->modificar();
    }

    public function actualizar() {
        //$this->model->update();
    }

    public function eliminar($id) {
        $this->model->indumentaria_id = $id;
        $this->model->delete();
    }

}


class IndumentariaHelper{

    public static function upload_imagefile($imagen_datos,$imagen_ppal_path) {
        $image_formats_permitted = array("image/png", "image/jpg", "image/jpeg");
        $info = getimagesize($imagen_datos["tmp_name"]);
        $extension = image_type_to_extension($info[2]);
        $mime = $info['mime'];

        self::create_dir_if_not_exists($imagen_ppal_path);

        if(!in_array($mime, $image_formats_permitted)) {
            return FALSE;
        }

        $name_path = $imagen_ppal_path . "principal{$extension}";
        move_uploaded_file($imagen_datos["tmp_name"], $name_path);
    }
    
    public static function create_dir_if_not_exists($dir) {
        if(!is_dir($dir)) mkdir($dir, 0755, TRUE);
    }
}

?>
